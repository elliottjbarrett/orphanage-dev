#include <SFML/Graphics.hpp>
#include <Thor/Animation.hpp>
#include <SWEngine.h>
#include <SWTestContext.h>

int main()
{
    SWEngine game;
    game.pushContext(std::unique_ptr<SWContext>(new SWTestContext(&game)));
    while (game.isRunning())
    {
        game.handleEvents();
        game.update();
        game.draw();
	}	
    return 0;
}
