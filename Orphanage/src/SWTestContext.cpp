#include "SWTestContext.h"

SWTestContext::SWTestContext(SWEngine *eng) :
 SWContext(eng), view(sf::FloatRect(0,0,1920,1080)), mapLoader("assets/") //XXX ZOOMED IN
{
    engine->getWindow()->setView(view);
    engine->getWindow()->setPosition(sf::Vector2i(0,0));

    systemManager = world.getSystemManager();
    entityManager = world.getEntityManager();
    //Init player
    player = &entityManager->create();
    player->addComponent(new Velocity(0,0,0,0));
    player->addComponent(new Transform(875,630));
    player->addComponent(new PlayerControlled());
    player->addComponent(new Animation("walkRight", "walkDown"));
    player->addComponent(new SpriteRef(std::unique_ptr<SWSprite>(engine->loadSprite("testSprite"))));
    player->addComponent(new CamFollow(true));
    player->addComponent(new Bounds(32,32));
    player->addComponent(new Shadow(16,28,12,1,0.5));
    player->addComponent(new HitBox(0,16,32,16));
    player->refresh();

    //Init werewolf
    werewolf = &entityManager->create();
    werewolf->addComponent(new Transform(150,150));
    werewolf->addComponent(new SpriteRef(std::unique_ptr<SWSprite>(engine->loadSprite("werewolf"))));
    werewolf->addComponent(new InteractResponse(IRT_MESSAGE, "LOL ITS A MESSAGE"));
    werewolf->addComponent(new PlayerControlled());
    werewolf->addComponent(new Velocity(0,0,0,0));
    werewolf->refresh();

    aiSystemNPC = (AISystemNPC*)&systemManager->setSystem(new AISystemNPC());
    particleSystem = (ParticleSystem*)&systemManager->setSystem(new ParticleSystem(engine->getWindow()));
    renderSystem = (RenderSystem*)&systemManager->setSystem(new RenderSystem(engine->getWindow()));
    inputSystem = (InputProcessingSystem*)&systemManager->setSystem(new InputProcessingSystem());
    movementSystem = (MovementSystem*)&systemManager->setSystem(new MovementSystem());
    animationSystem = (AnimationSystem*)&systemManager->setSystem(new AnimationSystem());
    collisionSystem = (CollisionSystem*)&systemManager->setSystem(new CollisionSystem(&mapLoader));
    cameraSystem = (CameraSystem*)&systemManager->setSystem(new CameraSystem(engine->getWindow()));
    mapSystem = (MapSystem*)&systemManager->setSystem(new MapSystem(engine, &mapLoader));
    dialogueSystem = (DialogueSystem*)&systemManager->setSystem(new DialogueSystem(engine->getWindow()));
    dayCycleSystem = (DayCycleSystem*)&systemManager->setSystem(new DayCycleSystem());
    shadowSystem = (ShadowSystem*)&systemManager->setSystem(new ShadowSystem(engine->getWindow()));
    weatherSystem = (WeatherSystem*)&systemManager->setSystem(new WeatherSystem(engine->getWindow()));
    interactSystem = (InteractSystem*)&systemManager->setSystem(new InteractSystem());

    cameraSystem->setWalkRect(sf::FloatRect(150,150,660,240));

    systemManager->initializeAll();
    mapSystem->spawnEntities();

}

SWTestContext::~SWTestContext()
{
    //dtor
}

void
SWTestContext::handleEvent(sf::Event *e)
{

}

void
SWTestContext::update(sf::Time delta)
{
    world.loopStart();
    world.setDelta(delta.asSeconds());
    particleSystem->update(delta);

    inputSystem->process();
    interactSystem->process();
    dialogueSystem->process();
    movementSystem->process();
    collisionSystem->process();
    animationSystem->process();
    mapSystem->process();
    cameraSystem->process();
    particleSystem->process();
    weatherSystem->process();
    aiSystemNPC->process();

    particleSystem->update(delta);
    weatherSystem->update(delta);
    dialogueSystem->update(delta);
}

void
SWTestContext::draw()
{
    //engine->getWindow()->draw(*testSprite->getSfSprite());
    //engine->getWindow()->draw(tileMap);
    //engine->getWindow()->draw(mapLoader);
    mapLoader.Draw(*engine->getWindow(), sf::Uint16(0));
    mapLoader.Draw(*engine->getWindow(), 1);
    shadowSystem->process();
    renderSystem->process();
    mapLoader.Draw(*engine->getWindow(), 2);
    mapLoader.Draw(*engine->getWindow(), 3);
    particleSystem->draw();
    weatherSystem->draw();
    dialogueSystem->draw();
}
