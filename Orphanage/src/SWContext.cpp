#include "SWContext.h"

SWContext::SWContext(SWEngine *eng) :
 engine(eng)
{
    //ctor
}

SWContext::~SWContext()
{
    //dtor
}

void
SWContext::init(int nLayers)
{
}

void
SWContext::handleEvent(sf::Event *e)
{
}

void
SWContext::update(sf::Time delta)
{
}

void
SWContext::draw()
{
}
