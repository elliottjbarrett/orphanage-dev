#include <SWEngine.h>

SWEngine::SWEngine() :
 window(sf::VideoMode(1920,1080), "SWEngine", sf::Style::None),
 running(true)
{
    window.setFramerateLimit(60);
    window.setVerticalSyncEnabled(true);
}

SWEngine::~SWEngine()
{
    //dtor
}

//Game loop methods
void
SWEngine::handleEvents()
{
    sf::Event e;
    while (window.pollEvent(e))
    {
        if (e.type == sf::Event::Closed)
        {
            quit();
        }
        //if (e.type == sf::Event::KeyEvent)

    }
}

void
SWEngine::update()
{
    sf::Time delta = clock.restart();
    contextStack.top()->update(delta);
    if (contextStack.top()->hasQuitRequest())
        quit();
    if (contextStack.top()->hasPopRequest())
        popContext();
}

void
SWEngine::draw()
{
    window.clear();
    contextStack.top()->draw();
    window.display();
}

//Context functionality
void
SWEngine::pushContext(std::unique_ptr<SWContext> con)
{
    contextStack.push(std::move(con));
}

void
SWEngine::popContext()
{
    contextStack.pop();
}

//Loading methods
std::unique_ptr<SWSprite>
SWEngine::loadSprite(std::string name)
{
    thor::FrameAnimation *frameAnim = 0;
    std::string fullPath, imageName, line, animName;
    int x, y, w, h, pos;
    float dur, absDur;

    fullPath = "assets/" + name + ".sdf";
    std::ifstream ifStream(fullPath);
    if (ifStream.is_open())
    {
        /* First line is the asset file */
        getline(ifStream, line);
        imageName = line;
        std::unique_ptr<SWSprite> sprite(new SWSprite(getTexture(line)));
        /* Create a new sprite */
        while (getline(ifStream,line))
        {
            if (line[0] != '[') //new animation
            {
                //Note: must add animation after all frames added
                if (frameAnim)
                {
                    sprite->addAnimation(animName, frameAnim, sf::seconds(absDur));
                }
                pos = line.find_last_of(':');
                animName = line.substr(0, pos);
                absDur = std::stof(line.substr(pos+1));

                frameAnim = new thor::FrameAnimation();
            }
            else //new frame
            {
                sscanf(line.c_str(),"[%d,%d,%d,%d,%f]",&x,&y,&w,&h,&dur);
                frameAnim->addFrame(dur, sf::IntRect(x,y,w,h));
            }
        }
        //End of file, still need to add the last animation
        sprite->addAnimation(animName, frameAnim, sf::seconds(absDur));
        return sprite;
    }

    return 0;
}

void
SWEngine::loadTexture(std::string filename)
{
    thor::ResourceKey<sf::Texture> key = thor::Resources::fromFile<sf::Texture>("assets/" + filename);
    textureCache.acquire(key);
}

std::shared_ptr<sf::Texture>
SWEngine::getTexture(std::string filename)
{
    thor::ResourceKey<sf::Texture> key = thor::Resources::fromFile<sf::Texture>("assets/" + filename);
    return textureCache.acquire(key);
}
