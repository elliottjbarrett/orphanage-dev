#include "SWSprite.h"

SWSprite::SWSprite(std::shared_ptr<sf::Texture> texture) :
 sprite(*texture)
{
    //texture->setSmooth(true);
}

SWSprite::~SWSprite()
{
    //dtor
}

void
SWSprite::setPosition(float x, float y)
{
    sprite.setPosition(x, y);
}

void
SWSprite::update(sf::Time delta)
{
    if (sprite.getTexture() == 0)
        return;

    animator.update(delta);
    animator.animate(sprite);
}

void
SWSprite::addAnimation(std::string name, thor::FrameAnimation *anim, sf::Time dur)
{
    animator.addAnimation(name, *anim, dur);
}

void
SWSprite::stopAnimation()
{
    animator.stopAnimation();
}

bool
SWSprite::isCurrentlyPlaying(std::string animName)
{
    if (animator.isPlayingAnimation())
        return animator.getPlayingAnimation() == animName;
    else
        return false;
}

void
SWSprite::setTexture(std::shared_ptr<sf::Texture> texture)
{
    sprite.setTexture(*texture);
}

void
SWSprite::setScale(float x, float y)
{
    sprite.setScale(x,y);
}

void
SWSprite::setScale(float v)
{
    sprite.setScale(v,v);
}

float
SWSprite::getScaleX()
{
    return sprite.getScale().x;
}

float
SWSprite::getScaleY()
{
    return sprite.getScale().y;
}

float
SWSprite::getWidth()
{
    return sprite.getGlobalBounds().width;
}

float
SWSprite::getHeight()
{
    return sprite.getGlobalBounds().height;
}
