#include <SWMessageBox.h>

SWMessageBox::SWMessageBox()
{
    if (!boxTexture.loadFromFile("assets/messageBox.png"))
        exit(-1);
    boxSprite.setTexture(boxTexture);
}

void
SWMessageBox::setMessage(std::string msg)
{
    messageText.setString(msg);
}

void
SWMessageBox::setSpeaker(std::string spk)
{
    nameText.setString(spk);
}

void
SWMessageBox::setFont(sf::Font *font)
{
    nameText.setFont(*font);
    messageText.setFont(*font);
}

void
SWMessageBox::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(boxSprite, states);
    target.draw(nameText, states);
    target.draw(messageText, states);
}
