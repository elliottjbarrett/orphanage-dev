#include <systems/InputProcessingSystem.h>

InputProcessingSystem::InputProcessingSystem()
{
    setComponentTypes<PlayerControlled,Velocity,Animation>();
}

void
InputProcessingSystem::initialize()
{
    pcMapper.init(*world);
    velocityMapper.init(*world);
    animationMapper.init(*world);
}

void
InputProcessingSystem::processEntity(artemis::Entity &e)
{
#define KeyPressed(x) (sf::Keyboard::isKeyPressed(x))
#define KeyIs(x) (sf::Keyboard::x)
#define ButtonPressed(x,y) (sf::Joystick::isButtonPressed(x,y))
#define JoyConnected(x) (sf::Joystick::isConnected(x)))
#define HasAxis(x) (sf::Joystick::hasAxis(x))
#define AxisIs(x) (sf::Joystick::Axis::x)
#define AxisValue(x,y) (sf::Joystick::getAxisPosition(x,y))
#define DEADZONE 15
#define MAXSPEED 1.5

    PlayerControlled *pc = pcMapper.get(e);
    Velocity *v = velocityMapper.get(e);
    Animation *anim = animationMapper.get(e);

    if (pc->suppress)
        return;

    bool run = ButtonPressed(0,2);

    if KeyPressed(KeyIs(Return))
        exit(0);
    if ButtonPressed(0,3)
        exit(0);

    float leftStickX = AxisValue(0,AxisIs(X));
    float leftStickY = AxisValue(0,AxisIs(Y));

    if (leftStickX > DEADZONE || leftStickX < -DEADZONE)
    {
        v->vx = MAXSPEED*((leftStickX > 0 ? leftStickX - DEADZONE : leftStickX + DEADZONE));
    }
    else
        v->vx = 0;

    if (leftStickY > DEADZONE || leftStickY < -DEADZONE)
        v->vy = MAXSPEED*((leftStickY > 0 ? leftStickY - DEADZONE : leftStickY + DEADZONE));
    else
        v->vy = 0;

    if (run)
    {
        v->vx *= 2;
        v->vy *= 2;
    }

    if (v->vx == 0 && v->vy == 0) //Select correct idle animation
    {
        if (anim->anim == "walkLeft")
            anim->anim = "idleLeft";
        else if (anim->anim == "walkRight")
            anim->anim = "idleRight";
        else if (anim->anim == "walkUp")
            anim->anim = "idleUp";
        else if (anim->anim == "walkDown")
            anim->anim = "idleDown";
        anim->stopRequest = true;
    }
    else
    {
        float absx = (v->vx > 0 ? v->vx : -v->vx);
        float absy = (v->vy > 0 ? v->vy : -v->vy);
        if (absx > absy) //Primarily walking horizontally
        {
            std::string nextAnim = (v->vx > 0 ? "walkRight" : "walkLeft");
            if (anim->anim != nextAnim)
            {
                anim->stopRequest = true;
                anim->anim = nextAnim;
            }
        }
        else //Primarily walking vertically
        {
            std::string nextAnim = (v->vy > 0 ? "walkDown" : "walkUp");
            if (anim->anim != nextAnim)
            {
                anim->stopRequest = true;
                anim->anim = nextAnim;
            }
        }
    }
    if (ButtonPressed(0,4))
    {
        float cX = -200;
        float cY = -200;
        float r = 10;
        float vX = 0;
        float vY = 150;
        float vDist = 30;
        float rotMin = 0;
        float rotMax = 360;
        float emLife = 5;
        float emRate = 5;
        float pLife = 3.5;
        std::string tex = "plusParticle";
        thor::ColorGradient gradient;
        gradient[0.0f] = sf::Color::Red;
        gradient[0.2f] = sf::Color::Green;
        gradient[0.4f] = sf::Color::White;
        gradient[0.8f] = sf::Color::Blue;
        gradient[1.0f] = sf::Color::Magenta;
        e.addComponent(new Particle(cX,cY,r,vX,vY,vDist,rotMin,rotMax,emLife,emRate,pLife,tex));
        e.addComponent(new FadeAffector(0.25,0.25));
        e.addComponent(new ColorAffector(gradient));
        e.addComponent(new ForceAffector(0, 120.0));
        e.refresh();
    }
    if (ButtonPressed(0,6))
    {
        e.addComponent(new Weather(WT_SNOW));
        e.refresh();
    }
    if (ButtonPressed(0,7))
    {
        e.addComponent(new InteractRequest());
        e.refresh();
    }
    if (ButtonPressed(0,8))
    {
        e.addComponent(new Weather(WT_NONE));
        e.refresh();
    }
#undef ButtonPressed
#undef KeyPressed
#undef KeyIs
#undef HasAxis
#undef AxisIs
#undef AxisValue
#undef JoyConnected
#undef DEADZONE
#undef MAXSPEED
}


