#include <systems/WeatherSystem.h>

WeatherSystem::WeatherSystem(sf::RenderWindow *w) :
    window(w), currentWeather(WT_NONE)
{
    setComponentTypes<Weather>();
    snowTexture.loadFromFile("assets/checkerParticle.png");
    system.setTexture(snowTexture);
}

void
WeatherSystem::initialize()
{
    weatherMapper.init(*world);
}

void
WeatherSystem::processEntity(artemis::Entity &e)
{
    Weather *w = weatherMapper.get(e);

    if (w->type == currentWeather)
        return;

    switch (w->type)
    {
    case WT_NONE:
        clearSystem();
        break;
    case WT_RAIN:

    case WT_SNOW:
        clearSystem();
        snow();
        break;
    case WT_FOG:

    default:
        clearSystem();
        break;
    }

    currentWeather = w->type;
    e.removeComponent<Weather>();
}

void
WeatherSystem::update(sf::Time delta)
{
    system.update(delta);
}

void
WeatherSystem::draw()
{
    //Should be unaffected by the view...
    sf::View prev = window->getView();
    window->setView(window->getDefaultView());
    window->draw(system);
    window->setView(prev);
}

void
WeatherSystem::clearSystem()
{
    system.clearAffectors();
    system.clearEmitters();
    system.clearParticles();
}

void
WeatherSystem::snow()
{
    //TODO: Put in logical place
    float snowRate = 2800;
    float snowLife = 6;
    float snowVX = -120;
    float snowVY = 120;
    float snowAccX = 0;
    float snowAccY = 30;
    float snowDeflection = 30;
    float snowFadeIn = 0.1;
    float snowFadeOut = 0.25;
    float snowRotVMin = 0;
    float snowRotVMax = 45;
    float snowRotMin = 0;
    float snowRotMax = 30;
    float snowScaleMin = 0.95;
    float snowScaleMax = 1.0;

    system.setTexture(snowTexture);
    thor::UniversalEmitter emitter;
    emitter.setEmissionRate(snowRate);
    emitter.setParticleLifetime(sf::seconds(snowLife));
    thor::Distribution<sf::Vector2f> dist = thor::Distributions::rect(sf::Vector2f(0,-20),sf::Vector2f(2500,10));
    emitter.setParticlePosition(dist);
    emitter.setParticleRotation(thor::Distributions::uniform(snowRotMin, snowRotMax));
    emitter.setParticleScale(thor::Distributions::rect(sf::Vector2f(snowScaleMin,snowScaleMin), sf::Vector2f(snowScaleMax,snowScaleMax)));
    emitter.setParticleRotationSpeed(thor::Distributions::uniform(snowRotVMin, snowRotVMax));
    emitter.setParticleVelocity(thor::Distributions::deflect(sf::Vector2f(snowVX, snowVY), snowDeflection));
    thor::ForceAffector grav(sf::Vector2f(snowAccX, snowAccY));

    thor::FadeAnimation fader(snowFadeIn, snowFadeOut);
    system.addAffector(thor::AnimationAffector(fader));
    system.addAffector(grav);
    system.addEmitter(emitter);

}

void
WeatherSystem::rain()
{
    //TODO
}

void
WeatherSystem::fog()
{
    //TODO
}
