#include <systems/RenderSystem.h>

RenderSystem::RenderSystem(sf::RenderWindow *window) :
    window(window)
{
    setComponentTypes<SpriteRef,Transform>();
}

void
RenderSystem::initialize()
{
    transformMapper.init(*world);
    spriteMapper.init(*world);
}

void
RenderSystem::processEntity(artemis::Entity &e)
{
    SpriteRef *sRef = spriteMapper.get(e);
    Transform *t = (Transform*)transformMapper.get(e);
    sf::Sprite *spr = sRef->sprite->getSfSprite();

    spr->setPosition(t->x,t->y);
    spr->setScale(t->scale_x,t->scale_y);
    window->draw(*spr);
}
