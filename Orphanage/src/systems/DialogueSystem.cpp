#include <systems/DialogueSystem.h>

DialogueSystem::DialogueSystem(sf::RenderWindow *w) :
    window(w), inputWait(sf::seconds(0)), inputDelay(sf::seconds(0.5))
{
    setComponentTypes<Message>();
    if (!font.loadFromFile("assets/lato-reg.ttf"))
        exit(-1);
}

void
DialogueSystem::initialize()
{
    messageMapper.init(*world);
}

void
DialogueSystem::processEntity(artemis::Entity &e)
{
    Message *m = messageMapper.get(e);

    speakerQueue.push(m->speaker);
    messageQueue.push(m->message);

    e.removeComponent<Message>();
    e.refresh();

    updateMessageBox();
}

void
DialogueSystem::update(sf::Time delta)
{
    inputWait += delta;

#define ButtonPressed(x,y) (sf::Joystick::isButtonPressed(x,y))
    if (speakerQueue.size() > 0 && acceptingInput())
    {
        if ButtonPressed(0,0)
        {
            inputWait = sf::seconds(0);
            popStacks();
        }
    }
#undef ButtonPressed
}

void
DialogueSystem::updateMessageBox()
{
    if (speakerQueue.size() > 0)
    {
        mBox.setMessage(messageQueue.front());
        mBox.setSpeaker(speakerQueue.front());
    }
}

void
DialogueSystem::draw()
{
    if (speakerQueue.size() == 0)
        return;
    sf::View prev = window->getView();
    window->setView(window->getDefaultView());

    //TODO: Draw message box
    window->draw(mBox);
    window->setView(prev);
}

bool
DialogueSystem::acceptingInput()
{
    return inputWait >= inputDelay;
}

void
DialogueSystem::popStacks()
{
    messageQueue.pop();
    speakerQueue.pop();
    updateMessageBox();
}
