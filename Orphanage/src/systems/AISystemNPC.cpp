#include <systems/AISystemNPC.h>

AISystemNPC::AISystemNPC()
{
    setComponentTypes<NPCTag>();
}

void
AISystemNPC::initialize()
{
    animationMapper.init(*world);
    velocityMapper.init(*world);
    wanderMapper.init(*world);
}

void
AISystemNPC::processEntity(artemis::Entity &e)
{
    AIWander *w = wanderMapper.get(e);
    Velocity *v = velocityMapper.get(e);
    Animation *a = animationMapper.get(e);

    if (w)
    {
        sf::Time delta = sf::seconds(world->getDelta());
        w->elapsed += delta;
        float randFrac = (float)(rand() % 100) / 100.0;
        float r = (float)rand();

        if (w->elapsed > w->curMax)
        {
            if (v->vx == 0 && v->vy == 0) //Wander
            {
                a->stopRequest = true;
                float vel = w->vMax*randFrac;
                v->vx = vel * sin(r);
                v->vy = vel * cos(r);
                w->elapsed = sf::seconds(0);
                w->curMax = sf::seconds(w->time + (randFrac - 0.5)*w->timeDiff);
            }
            else //Wait
            {
                a->stopRequest = true;
                v->vx = 0;
                v->vy = 0;
                w->elapsed = sf::seconds(0);
                w->curMax = sf::seconds(w->waitPeriod);
            }
        }
    }
}
