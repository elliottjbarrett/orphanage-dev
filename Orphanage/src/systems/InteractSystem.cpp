#include <systems/InteractSystem.h>

InteractSystem::InteractSystem()
{
    setComponentTypes<InteractRequest>();
}

void
InteractSystem::initialize()
{
    requestMapper.init(*world);
    responseMapper.init(*world);
    transformMapper.init(*world);
}

void
InteractSystem::processEntity(artemis::Entity &e)
{
    Transform *t = transformMapper.get(e);
    artemis::Entity *closest = findClosestInThreshold(t);

    if (closest)
    {
        InteractResponse *response = responseMapper.get(*closest);
        if (response)
        {
            switch (response->type)
            {
            case IRT_MESSAGE:
                //e.addComponent(loadFromFile(response->id));
                break;
            case IRT_WARP:
                break;
            }
        }
    }
    printf("XY:%f,%f\n",t->x,t->y);
    e.removeComponent<InteractRequest>();
    e.refresh();
}

artemis::Entity *
InteractSystem::findClosestInThreshold(Transform *t)
{
    Transform *tCur;
    InteractResponse *resp;
    artemis::Entity *closest, *current;
    float minDist = 10000000000; //TODO: make max float
    float curDist;
    float xOff, yOff;

    for (int i = 0; i < world->getEntityManager()->getEntities().getCount(); i++)
    {
        current = world->getEntityManager()->getEntities().get(i);
        tCur = transformMapper.get(*current);
        resp = responseMapper.get(*current);
        if (!resp || !tCur)
            continue;

        xOff = tCur->x - t->x;
        yOff = tCur->y - t->y;
        curDist = sqrt(xOff*xOff + yOff*yOff);
        if (curDist < minDist)
        {
            minDist = curDist;
            closest = current;
        }
        std::cout << curDist << '\n';
    }

    if (minDist <= interactDistanceThresh)
        return closest;
    else
        return 0;
}

Message*
InteractSystem::loadFromFile(std::string id)
{
    std::string fullPath = "assets/strings.sdb";
    std::string line;
    char buf[2048];
    std::ifstream ifStream(fullPath);

    if (ifStream.is_open())
    {
        while(getline(ifStream, line))
        {
            //buf = line.c_str();
        }
    }
}
