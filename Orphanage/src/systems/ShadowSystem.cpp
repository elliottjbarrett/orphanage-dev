#include <systems/ShadowSystem.h>

ShadowSystem::ShadowSystem(sf::RenderWindow *w) :
    window(w)
{
    setComponentTypes<Shadow,Transform>();
}

void
ShadowSystem::initialize()
{
    shadowMapper.init(*world);
    transformMapper.init(*world);
}

void
ShadowSystem::processEntity(artemis::Entity &e)
{
    Transform *t = transformMapper.get(e);
    Shadow *s = shadowMapper.get(e);

    sf::CircleShape shadow(s->radius);
    shadow.setScale(s->scaleX, s->scaleY);
    shadow.setOrigin(s->radius, s->radius);
    shadow.setPosition(t->x + s->xOffset, t->y + s->yOffset);
    shadow.setFillColor(sf::Color(0,0,0,125));

    window->draw(shadow);
}
