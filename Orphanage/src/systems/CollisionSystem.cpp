#include <systems/CollisionSystem.h>

CollisionSystem::CollisionSystem(tmx::MapLoader *ml) :
    mapLoader(ml)
{
    setComponentTypes<HitBox,Transform>();
}

void
CollisionSystem::initialize()
{
    transformMapper.init(*world);
    velocityMapper.init(*world);
    animationMapper.init(*world);
    hitBoxMapper.init(*world);
}

void
CollisionSystem::processEntities(artemis::ImmutableBag<artemis::Entity*> & bag)
{
    for (int i=0; i < bag.getCount(); i++)
    {
        artemis::Entity *e1 = bag.get(i);
        for (int j=i+1; j < bag.getCount(); j++)
        {
            artemis::Entity *e2 = bag.get(j);

            if (checkCollision(*e1, *e2))
            {
                //resolve collision properly
                resolveCollisionX(*e1, *e2);
                if (checkCollision(*e1, *e2))
                {
                    undoXResolution(*e1, *e2);
                    resolveCollisionY(*e1, *e2);
                    if (checkCollision(*e1, *e2))
                        resolveCollisionX(*e1, *e2);

                }
                if (checkCollision(*e1, *e2))
                    resolveCollisionX(*e1, *e2);
            }
        }
        resolveMapCollisions(*e1);
    }
}

void
CollisionSystem::processEntity(artemis::Entity &e)
{
}

bool
CollisionSystem::checkCollision(artemis::Entity &e1, artemis::Entity &e2)
{
    HitBox *h1 = hitBoxMapper.get(e2);
    HitBox *h2 = hitBoxMapper.get(e2);
    Transform *t1 = transformMapper.get(e1);
    Transform *t2 = transformMapper.get(e2);

    sf::FloatRect r1(t1->x + h1->xOffset, t1->y + h1->yOffset, h1->width, h1->height);
    sf::FloatRect r2(t2->x + h2->xOffset, t2->y + h2->yOffset, h2->width, h2->height);

    return r1.intersects(r2);
}

void
CollisionSystem::resolveMapCollisions(artemis::Entity &e)
{
    bool collision;
    Transform *t = transformMapper.get(e);
    HitBox *h = hitBoxMapper.get(e);
    Velocity *v = velocityMapper.get(e);

    sf::FloatRect hbox(t->x + h->xOffset, t->y + h->yOffset, h->width, h->height);
    sf::FloatRect objbox;
    for(auto layer = mapLoader->GetLayers().begin(); layer != mapLoader->GetLayers().end(); ++layer)
    {
        if (layer->name == "collisions")
        {
            for (auto object = layer->objects.begin(); object != layer->objects.end(); ++object)
            {
                objbox = object->GetAABB();
                if (hbox.intersects(objbox))
                {
                    hbox.left -= v->vx * world->getDelta(); //Resolve X
                    if (hbox.intersects(objbox)) //X resolution was insufficient
                    {
                        hbox.left += v->vx * world->getDelta(); //Undo X resolution
                        hbox.top -= v->vy * world->getDelta(); //Resolve Y
                        if (hbox.intersects(objbox)) //If still bad
                            hbox.left -= v->vx * world->getDelta(); //Resolve both X and Y
                    }
                    if (hbox.intersects(objbox))
                        hbox.left -= v->vx * world->getDelta();
                    t->x = hbox.left - h->xOffset;
                    t->y = hbox.top - h->yOffset;
                }
            }
        }
        else if (layer->name == "warps")
        {
            for (auto object = layer->objects.begin(); object != layer->objects.end(); ++object)
            {
                objbox = object->GetAABB();
                if (hbox.intersects(objbox))
                {
                    float x = stof(object->GetPropertyString("warpx"));
                    float y = stof(object->GetPropertyString("warpy"));
                    e.addComponent(new Warp(x,y, object->GetName()));
                    e.refresh();
                }
            }
        }
    }
}

void
CollisionSystem::resolveCollisionX(artemis::Entity &e1, artemis::Entity &e2)
{
    Velocity *v1 = velocityMapper.get(e1);
    Velocity *v2 = velocityMapper.get(e2);
    Transform *t1 = transformMapper.get(e1);
    Transform *t2 = transformMapper.get(e2);

    if (v1)
    {
        t1->x -= v1->vx * world->getDelta();
    }
    else if (v2)
    {
        t2->x -= v2->vx * world->getDelta();
    }
}

void
CollisionSystem::undoXResolution(artemis::Entity &e1, artemis::Entity &e2)
{
    Velocity *v1 = velocityMapper.get(e1);
    Velocity *v2 = velocityMapper.get(e2);
    Transform *t1 = transformMapper.get(e1);
    Transform *t2 = transformMapper.get(e2);

    if (v1)
    {
        t1->x += v1->vx * world->getDelta();
    }
    else if (v2)
    {
        t2->x += v2->vx * world->getDelta();
    }
}

void
CollisionSystem::resolveCollisionY(artemis::Entity &e1, artemis::Entity &e2)
{
    Velocity *v1 = velocityMapper.get(e1);
    Velocity *v2 = velocityMapper.get(e2);
    Transform *t1 = transformMapper.get(e1);
    Transform *t2 = transformMapper.get(e2);

    if (v1)
    {
        t1->y -= v1->vy * world->getDelta();
    }
    else if (v2)
    {
        t2->y -= v2->vy * world->getDelta();
    }
}
