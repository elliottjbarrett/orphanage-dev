#include <systems/HUDSystem.h>

HUDSystem::HUDSystem(sf::RenderWindow *window) :
    window(window)
{
}

void
HUDSystem::initialize()
{
}

void
HUDSystem::processEntity(artemis::Entity &e)
{
}

void
HUDSystem::update(sf::Time delta)
{
}

void
HUDSystem::draw()
{
}
