#include <systems/CameraSystem.h>

CameraSystem::CameraSystem(sf::RenderWindow *window) :
    window(window), view(sf::FloatRect(0,0,1920,1080))
{
    setComponentTypes<CamFollow,Transform,Bounds>();
    window->setView(view);
}

void
CameraSystem::initialize()
{
    camFollowMapper.init(*world);
    transformMapper.init(*world);
    boundsMapper.init(*world);
}

void
CameraSystem::processEntity(artemis::Entity &e)
{
    CamFollow *cf = camFollowMapper.get(e);
    Transform *t = transformMapper.get(e);
    Bounds *b = boundsMapper.get(e);

    if (!cf->active)
        return;

    sf::Vector2f vCenter = view.getCenter();
    sf::FloatRect globalWalkRect(vCenter.x - walkRect.width/2, vCenter.y - walkRect.height/2, walkRect.width, walkRect.height);
    sf::FloatRect globalEntityRect(t->x, t->y, b->width, b->height);

    resolveCamera(globalWalkRect, globalEntityRect);
}

void
CameraSystem::resolveCamera(sf::FloatRect walkRect, sf::FloatRect entityRect)
{
    //Resolve x
    if (entityRect.left < walkRect.left)
    {
        view.move(entityRect.left - walkRect.left, 0);
        window->setView(view);
    }
    else if (entityRect.left + entityRect.width > walkRect.left + walkRect.width)
    {
        view.move(entityRect.left + entityRect.width - (walkRect.left + walkRect.width), 0);
        window->setView(view);
    }
    //Resolve y
    if (entityRect.top < walkRect.top)
    {
        view.move(0, entityRect.top - walkRect.top);
        window->setView(view);
    }
    else if (entityRect.top + entityRect.height > walkRect.top + walkRect.height)
    {
        view.move(0, entityRect.top + entityRect.height - (walkRect.top + walkRect.height));
        window->setView(view);
    }
}
