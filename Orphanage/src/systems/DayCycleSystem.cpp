#include <systems/DayCycleSystem.h>

DayCycleSystem::DayCycleSystem()
{
}

DayCycleSystem::DayCycleSystem(sf::Time mLen, sf::Time dLen, sf::Time eLen, sf::Time nLen) :
    morningLength(mLen), dayLength(dLen), eveningLength(eLen), nightLength(nLen)
{
}

void
DayCycleSystem::initialize()
{
}

void
DayCycleSystem::update()
{
    if (paused)
        return;

    sf::Time delta = sf::seconds(world->getDelta());
    elapsed += delta;
}

void
DayCycleSystem::processEntity(artemis::Entity &e)
{
}

bool
DayCycleSystem::isMorning()
{
    return elapsed < morningLength
        && elapsed > sf::seconds(0);
}

bool
DayCycleSystem::isDay()
{
    return elapsed < morningLength + dayLength
        && elapsed > morningLength;
}

bool
DayCycleSystem::isEvening()
{
    return elapsed < morningLength + dayLength + eveningLength
        && elapsed > morningLength + dayLength;
}

bool
DayCycleSystem::isNight()
{
    return elapsed < morningLength + dayLength + eveningLength + nightLength
        && elapsed > morningLength + dayLength + eveningLength;
}
