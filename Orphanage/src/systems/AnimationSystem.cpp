#include <systems/AnimationSystem.h>

AnimationSystem::AnimationSystem()
{
    setComponentTypes<SpriteRef,Animation>();
}

void
AnimationSystem::initialize()
{
    spriteMapper.init(*world);
    animationMapper.init(*world);
}

void
AnimationSystem::processEntity(artemis::Entity &e)
{
    SpriteRef *spr = spriteMapper.get(e);
    Animation *anim = animationMapper.get(e);

    if (anim->stopRequest)
    {
        anim->stopRequest = false;
        spr->sprite->stopAnimation();
    }
    if (spr->sprite->isPlayingAnimation() == false)
    {
        if (anim->anim != "")
            spr->sprite->playAnimation(anim->anim);
        else
            spr->sprite->playAnimation(anim->defaultAnim);
    }
    spr->sprite->update(sf::seconds(world->getDelta()));
}
