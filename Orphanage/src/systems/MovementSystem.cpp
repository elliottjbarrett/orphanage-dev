#include <systems/MovementSystem.h>

MovementSystem::MovementSystem()
{
    setComponentTypes<Velocity,Transform>();
}

void
MovementSystem::initialize()
{
    velocityMapper.init(*world);
    transformMapper.init(*world);
    animationMapper.init(*world);
}

void
MovementSystem::processEntity(artemis::Entity &e)
{
    Animation *a = animationMapper.get(e);
    Transform *t = transformMapper.get(e);
    Velocity *v = velocityMapper.get(e);

    t->x += v->vx * world->getDelta();
    t->y += v->vy * world->getDelta();
    if (a)
    {

        bool idle = v->vx == 0 && v->vy == 0;
        bool right = v->vx > 0;
        bool down = v->vy > 0;

        if (idle)
        {
            if (a->anim == "walkLeft")
                a->anim = "idleLeft";
            else if (a->anim == "walkRight")
                a->anim = "idleRight";
            else if (a->anim == "walkUp")
                a->anim = "idleUp";
            else if (a->anim == "walkDown")
                a->anim = "idleDown";
            a->stopRequest = true;

            return;
        }

        if (right)
        {
            if (down)
            {
                a->anim = v->vx > v->vy ? "walkRight" : "walkDown";
            }
            else
            {
                a->anim = v->vx > -v->vy ? "walkRight" : "walkUp";
            }
        }
        else // left
        {
            if (down)
            {
                a->anim = -v->vx > v->vy ? "walkLeft" : "walkDown";
            }
            else
            {
                a->anim = -v->vx > -v->vy ? "walkLeft" : "walkUp";
            }
        }
    }
}
