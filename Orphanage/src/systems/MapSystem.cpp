#include <systems/MapSystem.h>

MapSystem::MapSystem(SWEngine *eng, tmx::MapLoader *ml) :
    engine(eng), mapLoader(ml)
{
    setComponentTypes<Warp,Transform>();
    mapLoader->Load("orphanage_interior.tmx");
}

void
MapSystem::initialize()
{
    transformMapper.init(*world);
    warpMapper.init(*world);
}

void
MapSystem::processEntity(artemis::Entity &e)
{
    Transform *t = transformMapper.get(e);
    Warp *w = warpMapper.get(e);

    t->x = w->x;
    t->y = w->y;

    mapLoader->Load(w->level + ".tmx");
    unloadEntities();
    spawnEntities();

    e.removeComponent<Warp>();
    e.refresh();
}


void
MapSystem::unloadEntities()
{
    for (auto ent = mapEntities.begin(); ent != mapEntities.end(); ent++)
    {
        world->getEntityManager()->remove(**ent);
    }

    mapEntities.clear();
}

void
MapSystem::spawnEntities()
{
    for(auto layer = mapLoader->GetLayers().begin(); layer != mapLoader->GetLayers().end(); ++layer)
    {
        if (layer->name == "entities")
        {
            for (auto object = layer->objects.begin(); object != layer->objects.end(); ++object)
            {
                spawnEntity(&*object);
            }
        }
    }
}

void
MapSystem::spawnEntity(tmx::MapObject *object)
{
    std::string file = object->GetPropertyString("file");
    std::string boundsString = object->GetPropertyString("bounds");
    float bX, bY;
    sf::Vector2f pos = object->GetPosition();

    artemis::Entity *e = &world->getEntityManager()->create();
    e->addComponent(new SpriteRef(std::unique_ptr<SWSprite>(engine->loadSprite(file))));
    addAITag(e, object->GetPropertyString("ai_type"));
    addAIWander(e, object->GetPropertyString("wander"));
    addAnimation(e, object->GetPropertyString("animation"));
    addBounds(e, object->GetPropertyString("bounds"));
    addHitBox(e, object->GetPropertyString("hitbox"));
    addShadow(e, object->GetPropertyString("shadow"));
    addVelocity(e, object->GetPropertyString("velocity"));
    addTransform(e, pos.x, pos.y);
    mapEntities.push_back(e);
    e->refresh();
}

void
MapSystem::addAITag(artemis::Entity *e, std::string s)
{
    //AI tags are of single string types
    if (s == "")
        return;

    if (s == "npc")
    {
        e->addComponent(new NPCTag());
        e->refresh();
    }
}

void
MapSystem::addAIWander(artemis::Entity *e, std::string s)
{
    //Wander component is of form vmax,tmax,tdiff
    if (s == "")
        return;

    float vm, tm, td;
    const char *str = s.c_str();
    sscanf(str, "%f,%f,%f", &vm, &tm, &td);
    e->addComponent(new AIWander(vm,tm,td));
    e->refresh();
}

void
MapSystem::addAnimation(artemis::Entity *e, std::string s)
{
    //Setting default anim only
    e->addComponent(new Animation("",s));
    e->refresh();
}


void
MapSystem::addBounds(artemis::Entity *e, std::string s)
{
    //Bounds string is of format w,h
    float x, y;
    const char *str = s.c_str();
    sscanf(str, "%f,%f", &x, &y);
    e->addComponent(new Bounds(x,y));
    e->refresh();
}

void
MapSystem::addHitBox(artemis::Entity *e, std::string s)
{
    //Hitbox string is of format xoff,yoff,w,h
    float x,y,w,h;
    const char *str = s.c_str();
    sscanf(str, "%f,%f,%f,%f", &x, &y, &w, &h);
    e->addComponent(new HitBox(x,y,w,y));
    e->refresh();
}

void
MapSystem::addShadow(artemis::Entity *e, std::string s)
{
    //Shadow string is of format xoff,yoff,radius
    float x,y,r;

    if (s.length() == 0)
        return;

    const char *str = s.c_str();
    sscanf(str, "%f,%f,%f", &x, &y, &r);
    e->addComponent(new Shadow(x,y,r));
    e->refresh();
}

void
MapSystem::addTransform(artemis::Entity *e, float x, float y)
{
    e->addComponent(new Transform(x,y));
    e->refresh();
}

void
MapSystem::addVelocity(artemis::Entity *e, std::string s)
{
    //Velocity string is in form: velx,vely
    float x,y;

    if (s.length() == 0)
        return;


    const char *str = s.c_str();
    sscanf(str, "%f,%f", &x, &y);
    e->addComponent(new Velocity(x,y));
    e->refresh();
}
