#include <systems/ParticleSystem.h>

ParticleSystem::ParticleSystem(sf::RenderWindow *w) :
    window(w), hasColor(false), hasFade(false), hasForce(false)
{
    setComponentTypes<Particle>();
    texture.loadFromFile("assets/checkerParticle.png");
    system.setTexture(texture);
}

void
ParticleSystem::initialize()
{
    particleMapper.init(*world);
    fadeMapper.init(*world);
    colorMapper.init(*world);
    forceMapper.init(*world);
}

void
ParticleSystem::clearAffectors()
{
    system.clearAffectors();
    hasFade = hasColor = hasForce = false;
}

void
ParticleSystem::clearEmitters()
{
    system.clearEmitters();
}

void
ParticleSystem::clearParticles()
{
    system.clearParticles();
}

void
ParticleSystem::update(sf::Time delta)
{
    system.update(delta);
}

void
ParticleSystem::processEntity(artemis::Entity &e)
{
    Particle *p = particleMapper.get(e);
    FadeAffector *f = fadeMapper.get(e);
    ColorAffector *c = colorMapper.get(e);
    ForceAffector *g = forceMapper.get(e);

    if (!p)
        return;

    thor::UniversalEmitter emitter;
    emitter.setEmissionRate(p->emissionRate);
    emitter.setParticleLifetime(thor::Distributions::uniform(sf::seconds(p->particleLifetime*0.9),sf::seconds(p->particleLifetime*1.1)));
    emitter.setParticlePosition(thor::Distributions::circle(sf::Vector2f(p->centerX,p->centerY), p->radius));
    emitter.setParticleVelocity(thor::Distributions::deflect(sf::Vector2f(p->velX,p->velY), p->velocityDistribution));
    emitter.setParticleRotation(thor::Distributions::uniform(p->rotationMin, p->rotationMax));
    system.addEmitter(emitter, sf::seconds(p->emissionLifetime));
    //texture.loadFromFile("assets/" + p->texture + ".png");
    //system.setTexture(texture);

    if (c && !hasColor)
    {
        hasColor = true;
        thor::ColorAnimation colorer(c->gradient);
        system.addAffector(thor::AnimationAffector(colorer));
        e.removeComponent<ColorAffector>();
    }

    if (f && !hasFade)
    {
        hasFade = true;
        thor::FadeAnimation fader(f->fadeIn, f->fadeOut);
        system.addAffector(thor::AnimationAffector(fader));
        e.removeComponent<FadeAffector>();
    }

    if (g && !hasForce)
    {
        hasForce = true;
        sf::Vector2f force(g->gravX, g->gravY);
        thor::ForceAffector grav(force);
        system.addAffector(grav);
        e.removeComponent<ForceAffector>();
    }

    e.removeComponent<Particle>();
    e.refresh();
}

void
ParticleSystem::draw()
{
    window->draw(system);
}
