#ifndef SWSPRITE_H
#define SWSPRITE_H

#include <memory>
#include <SFML/Graphics.hpp>
#include <Thor/Animation.hpp>

class SWSprite
{
    public:
        SWSprite(std::shared_ptr<sf::Texture> texture);
        virtual ~SWSprite();
        void addAnimation(std::string name, thor::FrameAnimation *anim, sf::Time dur);
        void setTexture(std::shared_ptr<sf::Texture> texture);

        //Accessors
        sf::Sprite * getSfSprite() { return &sprite; }
        float getScaleX();
        float getScaleY();
        float getWidth();
        float getHeight();

        //Mutators
        void setPosition(float x, float y);
        void setScale(float v);
        void setScale(float x, float y);

        //Animation control
        void update(sf::Time delta);
#if notyet
        void setDefaultAnimation(std::string animName);
        void playDefaultAnimation();
#endif
        bool isPlayingAnimation() { return animator.isPlayingAnimation(); }
        bool isCurrentlyPlaying(std::string animName);
        void stopAnimation();
        void playAnimation(std::string animName) { animator.playAnimation(animName); }
    protected:
    private:
        sf::Sprite sprite;
        thor::Animator<sf::Sprite, std::string> animator;
};

#endif // SWSPRITE_H
