#ifndef SWTESTCONTEXT_H
#define SWTESTCONTEXT_H

#include <iostream>
#include <SWContext.h>
#include <SWEngine.h>

#include <artemis/World.h>
#include <artemis/SystemManager.h>
#include <artemis/EntityManager.h>
#include <artemis/Entity.h>

#include <components/Transform.h>
#include <components/Velocity.h>
#include <components/PlayerControlled.h>
#include <components/SpriteRef.h>
#include <components/Shadow.h>
#include <components/HitBox.h>
#include <components/InteractRequest.h>
#include <components/InteractResponse.h>

#include <systems/AISystemNPC.h>
#include <systems/ParticleSystem.h>
#include <systems/RenderSystem.h>
#include <systems/InteractSystem.h>
#include <systems/InputProcessingSystem.h>
#include <systems/MovementSystem.h>
#include <systems/AnimationSystem.h>
#include <systems/CollisionSystem.h>
#include <systems/CameraSystem.h>
#include <systems/MapSystem.h>
#include <systems/WeatherSystem.h>
#include <systems/DayCycleSystem.h>
#include <systems/DialogueSystem.h>
#include <systems/ShadowSystem.h>

#include <sfml-tmxloader/tmx/MapLoader.h>
#include <sfml-tmxloader/tmx/MapLayer.h>
#include <sfml-tmxloader/tmx/MapObject.h>

class SWTestContext : public SWContext
{
    public:
        SWTestContext(SWEngine *eng);
        virtual ~SWTestContext();
        virtual void handleEvent(sf::Event *e);
        virtual void update(sf::Time delta);
        virtual void draw();
    protected:
    private:
        sf::View view;

        artemis::World world;
        artemis::SystemManager *systemManager;
        artemis::EntityManager *entityManager;
        artemis::Entity *player;
        artemis::Entity *werewolf;

        AISystemNPC *aiSystemNPC;
        ParticleSystem *particleSystem;
        RenderSystem *renderSystem;
        InputProcessingSystem *inputSystem;
        InteractSystem *interactSystem;
        MovementSystem *movementSystem;
        AnimationSystem *animationSystem;
        CollisionSystem *collisionSystem;
        CameraSystem *cameraSystem;
        MapSystem *mapSystem;
        DayCycleSystem *dayCycleSystem;
        DialogueSystem *dialogueSystem;
        ShadowSystem *shadowSystem;
        WeatherSystem *weatherSystem;

        //Tilemap testing
        tmx::MapLoader mapLoader;
        artemis::ComponentMapper<Transform> transformMapper;
};

#endif // SWTESTCONTEXT_H
