#ifndef SWCONTEXT_H
#define SWCONTEXT_H

#include <SFML/Graphics.hpp>
#include <vector>

class SWEngine;

class SWContext
{
    public:
        SWContext(SWEngine *eng);
        SWContext(SWContext &copy);
        SWContext &operator=(SWContext &rhs);
        ~SWContext();
        virtual void handleEvent(sf::Event *e);
        virtual void update(sf::Time delta);
        virtual void draw();

        bool hasQuitRequest() { return quitRequest; }
        bool hasPopRequest() { return popRequest; }
    protected:
        SWEngine *engine;
        bool quitRequest = false;
        bool popRequest = false;
        void init(int nLayers);
    private:
};

#endif // SWCONTEXT_H
