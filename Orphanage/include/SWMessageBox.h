#ifndef SWMESSAGEBOX_H
#define SWMESSAGEBOX_H

#include <SFML/Graphics.hpp>

class SWMessageBox : public sf::Drawable, sf::Transformable
{
    public:
        SWMessageBox();
        void setSpeaker(std::string spk);
        void setMessage(std::string msg);
        void setFont(sf::Font *font);
    private:
        sf::Sprite boxSprite;
        sf::Texture boxTexture;
        sf::Text nameText;
        sf::Text messageText;
        virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
};

#endif
