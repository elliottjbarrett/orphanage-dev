#ifndef ORPITEM_H
#define ORPITEM_H

#include <string>

class Item
{
    public:
        Item(std::string name, unsigned int n) : name(name), quantity(n) {}
    private:
        std::string name;
        unsigned int quantity;

};

#endif
