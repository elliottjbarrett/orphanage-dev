#ifndef WEATHERSYSTEM_H
#define WEATHERSYSTEM_H

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <Thor/Particles.hpp>
#include <Thor/Math/Distributions.hpp>
#include <Thor/Animation.hpp>
#include <artemis/ComponentMapper.h>
#include <artemis/Entity.h>
#include <artemis/EntityProcessingSystem.h>
#include <components/Weather.h>

class WeatherSystem : public artemis::EntityProcessingSystem
{
    public:
        WeatherSystem(sf::RenderWindow *w);
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
        void update(sf::Time delta);
        void draw();
    private:
        void clearSystem();
        void snow();
        void rain();
        void fog();

        artemis::ComponentMapper<Weather> weatherMapper;
        thor::ParticleSystem system;
        thor::UniversalEmitter snowEmitter;
        thor::UniversalEmitter rainEmitter;
        sf::RenderWindow *window;
        sf::Texture snowTexture;
        WeatherType currentWeather;
};

#endif
