#ifndef AISYSTEMNPC_H
#define AISYSTEMNPC_H

#include <artemis/ComponentMapper.h>
#include <artemis/EntityProcessingSystem.h>
#include <components/NPCTag.h>
#include <components/AIWander.h>
#include <components/Animation.h>
#include <components/Velocity.h>
#include <SFML/System/Time.hpp>
#include <math.h>

class AISystemNPC : public artemis::EntityProcessingSystem
{
    public:
        AISystemNPC();
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
    private:
        artemis::ComponentMapper<AIWander> wanderMapper;
        artemis::ComponentMapper<Animation> animationMapper;
        artemis::ComponentMapper<Velocity> velocityMapper;
};

#endif


