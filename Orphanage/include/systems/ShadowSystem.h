#ifndef SHADOWSYSTEM_H
#define SHADOWSYSTEM_H

#include <artemis/ComponentMapper.h>
#include <artemis/EntityProcessingSystem.h>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <components/Transform.h>
#include <components/Shadow.h>

class ShadowSystem : public artemis::EntityProcessingSystem
{
    public:
        ShadowSystem(sf::RenderWindow *w);
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
    private:
        sf::RenderWindow *window;
        artemis::ComponentMapper<Shadow> shadowMapper;
        artemis::ComponentMapper<Transform> transformMapper;
};

#endif
