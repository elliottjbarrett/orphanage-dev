#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H

#include <SFML/Graphics.hpp>
#include <artemis/ComponentMapper.h>
#include <artemis/EntityProcessingSystem.h>
#include <artemis/Entity.h>
#include <components/Transform.h>
#include <components/SpriteRef.h>

class RenderSystem : public artemis::EntityProcessingSystem
{
    public:
        RenderSystem(sf::RenderWindow* window);

        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);

    private:
        artemis::ComponentMapper<Transform> transformMapper;
        artemis::ComponentMapper<SpriteRef> spriteMapper;

        sf::RenderWindow *window;
};

#endif
