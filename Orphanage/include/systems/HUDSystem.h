#ifndef HUDSYSTEM_H
#define HUDSYSTEM_H

#include <artemis/EntityProcessingSystem.h>
#include <artemis/ComponentMapper.h>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

class HUDSystem : public artemis::EntityProcessingSystem
{
    public:
        HUDSystem(sf::RenderWindow *window);
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
        void update(sf::Time delta);
        void draw();
    private:
        sf::RenderWindow *window;
};

#endif
