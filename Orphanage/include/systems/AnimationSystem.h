#ifndef ANIMATIONSYSTEM_H
#define ANIMATIONSYSTEM_H

#include <artemis/EntityProcessingSystem.h>
#include <artemis/ComponentMapper.h>
#include <components/SpriteRef.h>
#include <components/Animation.h>

class AnimationSystem : public artemis::EntityProcessingSystem
{
    public:
        AnimationSystem();

        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);

    private:
        artemis::ComponentMapper<SpriteRef> spriteMapper;
        artemis::ComponentMapper<Animation> animationMapper;
};

#endif
