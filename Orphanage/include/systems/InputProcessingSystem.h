#ifndef INPUTPROCESSINGSYSTEM_H
#define INPUTPROCESSINGSYSTEM_H

#include <artemis/Entity.h>
#include <artemis/EntityProcessingSystem.h>
#include <artemis/ComponentMapper.h>
#include <components/PlayerControlled.h>
#include <components/Particle.h>
#include <components/Velocity.h>
#include <components/Animation.h>
#include <components/ColorAffector.h>
#include <components/ForceAffector.h>
#include <components/InteractRequest.h>
#include <components/FadeAffector.h>
#include <components/Warp.h>
#include <components/Weather.h>
#include <SFML/Window.hpp>


class InputProcessingSystem : public artemis::EntityProcessingSystem
{
    public:
        InputProcessingSystem();

        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);

    private:
        artemis::ComponentMapper<PlayerControlled> pcMapper;
        artemis::ComponentMapper<Velocity> velocityMapper;
        artemis::ComponentMapper<Animation> animationMapper;
};

#endif
