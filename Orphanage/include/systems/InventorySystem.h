#ifndef INVENTORYSYSTEM_H
#define INVENTORYSYSTEM_H

#include <orp/Item.h>
#include <artemis/EntityProcessingSystem.h>
#include <artemis/Entity.h>
#include <map>
class InventorySystem : public artemis::EntityProcessingSystem
{
    public:
        InventorySystem();
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
    private:
        std::map<std::string, Item> itemMap;
};

#endif


