#ifndef MOVEMENTSYSTEM_H
#define MOVEMENTSYSTEM_H

#include <artemis/EntityProcessingSystem.h>
#include <artemis/ComponentMapper.h>
#include <components/Animation.h>
#include <components/Transform.h>
#include <components/Velocity.h>

class MovementSystem : public artemis::EntityProcessingSystem
{
    public:
        MovementSystem();
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);

    private:
        artemis::ComponentMapper<Velocity> velocityMapper;
        artemis::ComponentMapper<Transform> transformMapper;
        artemis::ComponentMapper<Animation> animationMapper;
};

#endif
