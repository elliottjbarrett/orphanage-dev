#ifndef DIALOGUESYSTEM_H
#define DIALOGUESYSTEM_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Joystick.hpp>
#include <artemis/Entity.h>
#include <artemis/EntityProcessingSystem.h>
#include <artemis/ComponentMapper.h>
#include <SWMessageBox.h>
#include <components/Message.h>
#include <queue>

class DialogueSystem : public artemis::EntityProcessingSystem
{
    public:
        DialogueSystem(sf::RenderWindow *w);
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
        void update(sf::Time delta);
        void draw();
    private:
        bool acceptingInput();
        void popStacks();
        void updateMessageBox();

        SWMessageBox mBox;
        artemis::ComponentMapper<Message> messageMapper;
        sf::RenderWindow *window;
        std::queue<std::string> speakerQueue;
        std::queue<std::string> messageQueue;
        sf::Time inputWait;
        sf::Time inputDelay;
        sf::Font font;
};

#endif


