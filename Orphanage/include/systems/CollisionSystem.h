#ifndef COLLISIONSYSTEM_H
#define COLLISIONSYSTEM_H

#include <SFML/Graphics/Rect.hpp>
#include <sfml-tmxloader/tmx/MapLoader.h>
#include <artemis/EntityProcessingSystem.h>
#include <artemis/Entity.h>
#include <artemis/ComponentMapper.h>
#include <components/Transform.h>
#include <components/Velocity.h>
#include <components/Animation.h>
#include <components/HitBox.h>
#include <components/Warp.h>

class CollisionSystem : public artemis::EntityProcessingSystem
{
    public:
        CollisionSystem(tmx::MapLoader *ml);

        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
        virtual void processEntities(artemis::ImmutableBag<artemis::Entity*> & bag);

    private:
        artemis::ComponentMapper<Transform> transformMapper;
        artemis::ComponentMapper<Velocity> velocityMapper;
        artemis::ComponentMapper<Animation> animationMapper;
        artemis::ComponentMapper<HitBox> hitBoxMapper;

        bool checkCollision(artemis::Entity &e1, artemis::Entity &e2);
        void resolveCollisionX(artemis::Entity &e1, artemis::Entity &e2);
        void resolveCollisionY(artemis::Entity &e1, artemis::Entity &e2);
        void undoXResolution(artemis::Entity &e1, artemis::Entity &e2);
        void resolveMapCollisions(artemis::Entity &e);
        tmx::MapLoader *mapLoader;
};

#endif
