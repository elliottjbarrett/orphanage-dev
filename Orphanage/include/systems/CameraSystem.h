#ifndef CAMERASYSTEM_H
#define CAMERASYSTEM_H

#include <artemis/EntityProcessingSystem.h>
#include <artemis/ComponentMapper.h>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include <components/CamFollow.h>
#include <components/Transform.h>
#include <components/Bounds.h>

class CameraSystem : public artemis::EntityProcessingSystem
{
    public:
        CameraSystem(sf::RenderWindow *window);
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
        void setWalkRect(sf::FloatRect rect) { walkRect = rect; }

    private:
        artemis::ComponentMapper<CamFollow> camFollowMapper;
        artemis::ComponentMapper<Transform> transformMapper;
        artemis::ComponentMapper<Bounds> boundsMapper;
        sf::FloatRect walkRect;
        sf::RenderWindow *window;
        sf::View view;

        void resolveCamera(sf::FloatRect walkRect, sf::FloatRect entityRect);
};

#endif
