#ifndef INTERACTSYSTEM_H
#define INTERACTSYSTEM_H

#include <artemis/ComponentMapper.h>
#include <artemis/EntityManager.h>
#include <artemis/EntityProcessingSystem.h>
#include <artemis/Entity.h>
#include <components/InteractRequest.h>
#include <components/InteractResponse.h>
#include <components/Message.h>
#include <components/Transform.h>
#include <math.h>
#include <fstream>

class InteractSystem : public artemis::EntityProcessingSystem
{
    public:
        InteractSystem();
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
    private:
        artemis::Entity * findClosestInThreshold(Transform *t);
        Message *loadFromFile(std::string id); //TODO: Move to a utility class
        artemis::ComponentMapper<InteractRequest> requestMapper;
        artemis::ComponentMapper<InteractResponse> responseMapper;
        artemis::ComponentMapper<Transform> transformMapper;
        bool acceptingRequests = true;
        float interactDistanceThresh = 50.0;
};

#endif

