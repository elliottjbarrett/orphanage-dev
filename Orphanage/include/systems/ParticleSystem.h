#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

#include <artemis/ComponentMapper.h>
#include <artemis/EntityProcessingSystem.h>
#include <artemis/Entity.h>
#include <Thor/Animation.hpp>
#include <Thor/Particles.hpp>
#include <Thor/Math.hpp>
#include <SFML/Graphics.hpp>
#include <components/Particle.h>
#include <components/FadeAffector.h>
#include <components/ForceAffector.h>
#include <components/ColorAffector.h>

class ParticleSystem : public artemis::EntityProcessingSystem
{
    public:
        ParticleSystem(sf::RenderWindow *w);
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
        void update(sf::Time delta);
        void clearAffectors();
        void clearEmitters();
        void clearParticles();
        void draw();
    private:
        thor::ParticleSystem system;
        artemis::ComponentMapper<Particle> particleMapper;
        artemis::ComponentMapper<FadeAffector> fadeMapper;
        artemis::ComponentMapper<ForceAffector> forceMapper;
        artemis::ComponentMapper<ColorAffector> colorMapper;
        sf::RenderWindow *window;
        sf::Texture texture;

        bool hasColor;
        bool hasFade;
        bool hasForce;
};

#endif
