#ifndef DAYCYCLESYSTEM_H
#define DAYCYCLESYSTEM_H

#include <SFML/System/Time.hpp>
#include <artemis/World.h>
#include <artemis/EntityProcessingSystem.h>
#include <artemis/Entity.h>

class DayCycleSystem : public artemis::EntityProcessingSystem
{
    public:
        DayCycleSystem();
        DayCycleSystem(sf::Time mLen, sf::Time dlen, sf::Time eLen, sf::Time nLen);
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
        void update();
        void setPaused(bool p) { paused = p; };
    private:
        sf::Time morningLength;
        sf::Time dayLength;
        sf::Time eveningLength;
        sf::Time nightLength;
        sf::Time elapsed;

        bool isMorning();
        bool isDay();
        bool isEvening();
        bool isNight();

        bool paused;
};

#endif
