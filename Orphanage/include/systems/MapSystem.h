#ifndef MAPSYSTEM_H
#define MAPSYSTEM_H

#include <sfml-tmxloader/tmx/MapLoader.h>
#include <artemis/Entity.h>
#include <artemis/EntityProcessingSystem.h>
#include <artemis/ComponentMapper.h>
#include <components/AIWander.h>
#include <components/Animation.h>
#include <components/Bounds.h>
#include <components/HitBox.h>
#include <components/NPCTag.h>
#include <components/Shadow.h>
#include <components/SpriteRef.h>
#include <components/Transform.h>
#include <components/Velocity.h>
#include <components/Warp.h>
#include <SWEngine.h>

class MapSystem : public artemis::EntityProcessingSystem
{
    public:
        MapSystem(SWEngine *eng, tmx::MapLoader *ml);
        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
        void spawnEntities();
        void unloadEntities();
    private:
        SWEngine *engine;
        tmx::MapLoader *mapLoader;
        artemis::ComponentMapper<Warp> warpMapper;
        artemis::ComponentMapper<Transform> transformMapper;

        void spawnEntity(tmx::MapObject *obj);
        std::vector<artemis::Entity *> mapEntities;

        void addAITag(artemis::Entity *e, std::string s);
        void addAIWander(artemis::Entity *e, std::string s);
        void addAnimation(artemis::Entity *e, std::string s);
        void addBounds(artemis::Entity *e, std::string s);
        void addHitBox(artemis::Entity *e, std::string s);
        void addShadow(artemis::Entity *e, std::string s);
        void addTransform(artemis::Entity *e, float x, float y);
        void addVelocity(artemis::Entity *e, std::string s);
};

#endif
