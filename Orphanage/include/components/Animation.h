#ifndef ANIMATIONCOMPONENT_H
#define ANIMATIONCOMPONENT_H

#include <artemis/Component.h>

class Animation : public artemis::Component
{
    public:
        std::string anim;
        std::string defaultAnim;
        bool stopRequest = false;

        Animation(std::string anim, std::string defaultAnim) :
            anim(anim), defaultAnim(defaultAnim) {}
};

#endif
