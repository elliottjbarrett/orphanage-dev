#ifndef PARTICLECOMPONENT_H
#define PARTICLECOMPONENT_H

#include <artemis/Component.h>

class Particle : public artemis::Component
{
    public:
        Particle();
        Particle(float cX, float cY, float r, float vX, float vY, float vDist, float rotMin,
                 float rotMax, float emLife, float emRate, float pLife, std::string tex) :
            centerX(cX), centerY(cY), radius(r),
            velX(vX), velY(vY), velocityDistribution(vDist),
            rotationMin(rotMin), rotationMax(rotMax),
            emissionLifetime(emLife), emissionRate(emRate),
            particleLifetime(pLife),
            texture(tex) {};
        float centerX, centerY, radius;
        float velX, velY, velocityDistribution;
        float rotationMin, rotationMax;
        float emissionLifetime, emissionRate;
        float particleLifetime;
        std::string texture;
};

#endif
