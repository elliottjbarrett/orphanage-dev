#ifndef WARPCOMPONENT_H
#define WARPCOMPONENT_H

#include <artemis/Component.h>

class Warp : public artemis::Component
{
    public:
       Warp(float x, float y, std::string level) :
            x(x), y(y), level(level) {}

        float x;
        float y;
        std::string level;
};

#endif
