#ifndef AIWANDER_H
#define AIWANDER_H

#include <artemis/Component.h>
#include <SFML/System/Time.hpp>

class AIWander : public artemis::Component
{
    public:
        AIWander(float vMax, float tMax, float tDiff) :
            vMax(vMax), time(tMax), timeDiff(tDiff) {}
        float vMax, time, timeDiff;
        sf::Time elapsed, curMax;
        float waitPeriod = 3;
};

#endif

