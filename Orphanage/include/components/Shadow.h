#ifndef SHADOW_H
#define SHADOW_H

#include <artemis/Component.h>

class Shadow : public artemis::Component
{
    public:
        Shadow(float xOff, float yOff, float r, float sx = 1, float sy = 0.5) :
            xOffset(xOff), yOffset(yOff), radius(r), scaleX(sx), scaleY(sy) {}

        float xOffset, yOffset, radius;
        float scaleX, scaleY;
};

#endif
