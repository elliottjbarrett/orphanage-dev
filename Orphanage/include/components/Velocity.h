#ifndef VELOCITYCOMPONENT_H
#define VELOCITYCOMPONENT_H

#include "artemis/Component.h"

class Velocity : public artemis::Component
{
    public:
        float vx, vy;
        float ax, ay;

        Velocity(float vX, float vY, float aX=0, float aY=0) :
            vx(vX), vy(vY), ax(aX), ay(aY)
        {
        };

};

#endif
