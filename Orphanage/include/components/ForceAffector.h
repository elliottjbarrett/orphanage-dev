#ifndef FORCEAFFECTORCOMPONENT_H
#define FORCEAFFECTORCOMPONENT_H

#include <artemis/Component.h>

class ForceAffector : public artemis::Component
{
    public:
        ForceAffector(float gX, float gY) : gravX(gX), gravY(gY) {}
        float gravX, gravY;
};

#endif

