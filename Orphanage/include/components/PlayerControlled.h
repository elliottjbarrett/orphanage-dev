#ifndef PLAYERCONTROLLED_H
#define PLAYERCONTROLLED_H

#include <artemis/Component.h>

class PlayerControlled : public artemis::Component
{
    public:
        bool suppress = false;
};

#endif
