#ifndef FADEAFFECTORCOMPONENT_H
#define FADEAFFECTORCOMPONENT_H

#include <artemis/Component.h>

class FadeAffector : public artemis::Component
{
    public:
        FadeAffector(float in, float out) : fadeIn(in), fadeOut(out) {}
        float fadeIn, fadeOut;
};

#endif
