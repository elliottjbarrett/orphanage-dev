#ifndef COLORAFFECTORCOMPONENT_H
#define COLORAFFECTORCOMPONENT_H

#include <Thor/Graphics/ColorGradient.hpp>
#include <artemis/Component.h>

class ColorAffector : public artemis::Component
{
    public:
        ColorAffector(thor::ColorGradient g) : gradient(g) {}
        thor::ColorGradient gradient;
};

#endif
