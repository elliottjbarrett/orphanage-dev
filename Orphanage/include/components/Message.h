#ifndef MESSAGECOMPONENT_H
#define MESSAGECOMPONENT_H

#include <artemis/Component.h>

class Message : public artemis::Component
{
    public:
        Message(std::string speaker, std::string message) :
            speaker(speaker), message(message) {}

        std::string speaker;
        std::string message;
};

#endif

