#ifndef HITBOX_H
#define HITBOX_H

#include <artemis/Component.h>

class HitBox : public artemis::Component
{
    public:
        HitBox(float x, float y, float w, float h) :
            xOffset(x), yOffset(y), width(w), height(h) {}
        float xOffset, yOffset, width, height;
};

#endif

