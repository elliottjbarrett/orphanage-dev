#ifndef TRANSFORMCOMPONENT_H
#define TRANSFORMCOMPONENT_H

#include "artemis/Component.h"

class Transform : public artemis::Component
{
    public:
        float x;
        float y;
        float rotation;
        float scale_x;
        float scale_y;

        Transform(float X, float Y) :
            x(X),
            y(Y),
            rotation(0),
            scale_x(1),
            scale_y(1)
        {
        }
};

#endif
