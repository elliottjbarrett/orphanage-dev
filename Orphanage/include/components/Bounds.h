#ifndef BOUNDS_H
#define BOUNDS_H

#include <artemis/Component.h>
#include <SFML/Graphics/Rect.hpp>

class Bounds : public artemis::Component
{
    public:
        Bounds(float w, float h) : width(w), height(h) {}

        float width;
        float height;
};

#endif
