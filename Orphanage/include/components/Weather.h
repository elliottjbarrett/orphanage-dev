#ifndef WEATHERCOMPONENT_H
#define WEATHERCOMPONENT_H

#include <artemis/Component.h>

enum WeatherType {
    WT_NONE,
    WT_SNOW,
    WT_RAIN,
    WT_FOG
};

class Weather : public artemis::Component
{
    public:
        Weather(WeatherType t) : type(t) {}
        WeatherType type;
};

#endif
