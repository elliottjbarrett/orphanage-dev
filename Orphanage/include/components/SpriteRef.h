#ifndef SPRITEREF_H
#define SPRITEREF_H

#include <memory>
#include <artemis/Component.h>
#include <SWSprite.h>

class SpriteRef : public artemis::Component
{
    public:
        SpriteRef(std::unique_ptr<SWSprite> spr)
        {
            sprite = std::move(spr);
        }

        std::unique_ptr<SWSprite> sprite;
};

#endif
