#ifndef INTERACTRESPONSECOMPONENT_H
#define INTERACTRESPONSECOMPONENT_H

#include <artemis/Component.h>

enum InteractResponseType
{
    IRT_NONE,
    IRT_MESSAGE,
    IRT_WARP,
    IRT_INVALID
};

class InteractResponse : public artemis::Component
{
    public:
        InteractResponse(InteractResponseType t, std::string id) :
            type(t), id(id) {}
        InteractResponseType type;
        std::string id;
};

#endif

