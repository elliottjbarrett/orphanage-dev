#ifndef CAMFOLLOW_H
#define CAMFOLLOW_H

#include <artemis/Component.h>

class CamFollow : public artemis::Component
{
    public:
        CamFollow(bool isActive) :
            active(isActive) {}

        bool active;
};

#endif
