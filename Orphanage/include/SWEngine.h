#ifndef SWENGINE_H
#define SWENGINE_H

#include <SFML/Graphics.hpp>
#include <Thor/Resources.hpp>
#include <SWContext.h>
#include <SWSprite.h>
#include <fstream>
#include <stack>
#include <iostream>


class SWEngine
{
    public:
        SWEngine();
        virtual ~SWEngine();

        //Game loop functionality
        void handleEvents();
        void update();
        void draw();
        bool isRunning() { return running; }
        void quit() { running = false; }

        //Context functionality
        void pushContext(std::unique_ptr<SWContext> context);
        void popContext();

        //Loading functionality
        std::unique_ptr<SWSprite> loadSprite(std::string name);
        void loadTexture(std::string fileName);
        std::shared_ptr<sf::Texture> getTexture(std::string fileName);

        //Accessors
        sf::RenderWindow * getWindow() { return &window; }

        //Condition pool methods
        void addCondition(std::string cond) { conditionPool.push_back(cond); }
        bool hasCondition(std::string cond) { return std::find(conditionPool.begin(), conditionPool.end(), cond) != conditionPool.end(); }
        void removeCondition(std::string cond) { conditionPool.erase(std::find(conditionPool.begin(), conditionPool.end(), cond)); }

    protected:
    private:
        sf::RenderWindow window;
        sf::Clock clock;
        thor::ResourceCache<sf::Texture> textureCache;
        bool running;

        std::stack<std::unique_ptr<SWContext>> contextStack;
        std::vector<std::string> conditionPool;
};

#endif // SWENGINE_H
